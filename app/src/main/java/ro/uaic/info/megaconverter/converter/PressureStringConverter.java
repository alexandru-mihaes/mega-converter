package ro.uaic.info.megaconverter.converter;

class PressureStringConverter {
    public static Double convertValueFromTo(String fromTextMeasurement, String toTextMeasurement, String fromValueString) {
        Double fromValue = Double.parseDouble(fromValueString);
        switch (fromTextMeasurement) {
            case "Pascal": {
                switch (toTextMeasurement) {
                    case "Pascal": {
                        return fromValue;
                    }
                    case "Bar": {
                        return PressureConverter.pascalToBar(fromValue);
                    }
                    case "mmHg": {
                        return PressureConverter.pascalToMmhg(fromValue);
                    }
                }
                break;
            }
            case "Bar": {
                switch (toTextMeasurement) {
                    case "Pascal": {
                        return PressureConverter.barToPascal(fromValue);
                    }
                    case "Bar": {
                        return fromValue;
                    }
                    case "mmHg": {
                        return PressureConverter.barToMmhg(fromValue);
                    }
                }
                break;
            }
            case "mmHg": {
                switch (toTextMeasurement) {
                    case "Pascal": {
                        return PressureConverter.mmhgToPascal(fromValue);
                    }
                    case "Bar": {
                        return PressureConverter.mmhgToBar(fromValue);
                    }
                    case "mmHg": {
                        return fromValue;
                    }
                }
                break;
            }
        }
        return 0.0;
    }
}
