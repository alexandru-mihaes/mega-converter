package ro.uaic.info.megaconverter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import ro.uaic.info.megaconverter.converter.CurrencyConverter;
import ro.uaic.info.megaconverter.converter.MainConverter;
import ro.uaic.info.megaconverter.service.CurrencyService;
import ro.uaic.info.megaconverter.utils.MeasurementsStrings;

public class MainActivity extends AppCompatActivity {
    private RelativeLayout relativeLayoutUpperSquare, relativeLayoutLowerSquare;
    private Spinner spinnerMode, spinnerUpper, spinnerLower;
    private EditText editTextUpper, editTextLower;
    private ImageButton imageBtnSwitch;

    private String mode;

    private View.OnClickListener imageBtnSwitchClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // reversing values
            int upperItemId = spinnerUpper.getSelectedItemPosition();
            int lowerItemId = spinnerLower.getSelectedItemPosition();
            spinnerUpper.setSelection(lowerItemId);
            spinnerLower.setSelection(upperItemId);
            String editTextUpperString = editTextUpper.getText().toString();
            String editTextLowerString = editTextLower.getText().toString();
            editTextUpper.setText(editTextLowerString);
            editTextLower.setText(editTextUpperString);
        }
    };

    private AdapterView.OnItemSelectedListener spinnerModeItemClickListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            updateMode();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
        }
    };

    private AdapterView.OnItemSelectedListener spinnerItemClickListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            updateLowerEditText();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setLayoutHeights();
        fillModeSpinner();
        editTextUpper = findViewById(R.id.edit_text_upper);
        editTextLower = findViewById(R.id.edit_text_lower);
        editTextLower.setFocusable(false);
        editTextUpper.addTextChangedListener(textChangeListenerUpper);
        mode = "Length";
        updateMode();
        imageBtnSwitch = findViewById(R.id.btn_switch);
        imageBtnSwitch.setOnClickListener(imageBtnSwitchClickListener);
        setSpinnersListener();

        // make currency GET request every 10 minutes
        final int MINUTES = 10;
        Timer timer = new Timer();
        final Activity activity = this;
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                CurrencyService currencyService = new CurrencyService(activity);
                currencyService.makeRequest();
            }
        }, 0, 1000 * 60 * MINUTES); // 1000 milliseconds in a second * 60 per minute * the MINUTES variable
    }

    private void setSpinnersListener() {
        spinnerUpper.setOnItemSelectedListener(spinnerItemClickListener);
        spinnerLower.setOnItemSelectedListener(spinnerItemClickListener);
        spinnerMode.setOnItemSelectedListener(spinnerModeItemClickListener);
    }

    private void setLayoutHeights() {
        relativeLayoutUpperSquare = findViewById(R.id.upper_square);
        relativeLayoutLowerSquare = findViewById(R.id.lower_square);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        relativeLayoutUpperSquare.getLayoutParams().height = displayMetrics.heightPixels / 2;
        relativeLayoutLowerSquare.getLayoutParams().height = displayMetrics.heightPixels / 2;
    }

    private void fillModeSpinner() {
        spinnerMode = findViewById(R.id.spinner_mode);
        List<String> modes = MeasurementsStrings.modes;
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.spinner_white, modes
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMode.setAdapter(adapter);
    }

    private void updateMode() {
        mode = spinnerMode.getSelectedItem().toString();
        editTextUpper.setText("");
        editTextLower.setText("");
        fillUpperSpinner();
        fillLowerSpinner();
    }

    private void fillUpperSpinner() {
        spinnerUpper = findViewById(R.id.spinner_upper);
        List<String> measures = MeasurementsStrings.getMeasurementsByMode(mode);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.spinner_white, measures
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerUpper.setAdapter(adapter);
    }

    private void fillLowerSpinner() {
        spinnerLower = findViewById(R.id.spinner_lower);
        List<String> measures = MeasurementsStrings.getMeasurementsByMode(mode);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.spinner_blue, measures
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLower.setAdapter(adapter);
    }

    private TextWatcher textChangeListenerUpper = new TextWatcher() {
        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start,
                                      int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start,
                                  int before, int count) {
            if(!(s.toString()).isEmpty()) {
                updateLowerEditText();
            }
        }
    };

    private void updateLowerEditText() {
        String fromTextMeasurement = spinnerUpper.getSelectedItem().toString();
        String toTextMeasurement = spinnerLower.getSelectedItem().toString();
        String fromValueString = editTextUpper.getText().toString();
        if (!fromValueString.isEmpty()) {
            Double toValue;
            if(mode.equals("Currency")) {
                toValue = CurrencyConverter.convertValue(this, fromTextMeasurement, toTextMeasurement, fromValueString);
            } else {
                toValue = MainConverter.convertValueFromToByMode(fromTextMeasurement, toTextMeasurement, fromValueString, mode);
            }
            String toValueString = String.format("%.12f", toValue);
            editTextLower.setText(toValueString);
        } else {
            editTextLower.setText("");
        }
    }
}
