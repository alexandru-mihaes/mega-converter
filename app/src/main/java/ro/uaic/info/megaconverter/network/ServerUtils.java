package ro.uaic.info.megaconverter.network;

public class ServerUtils {
    public static final String DOMAIN = "https://api.exchangeratesapi.io";

    public static String getLatestForeignExchangeRates() {
        return DOMAIN + "/latest";
    }
}
