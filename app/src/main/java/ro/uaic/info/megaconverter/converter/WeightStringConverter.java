package ro.uaic.info.megaconverter.converter;

class WeightStringConverter {
    public static Double convertValueFromTo(String fromTextMeasurement, String toTextMeasurement, String fromValueString) {
        Double fromValue = Double.parseDouble(fromValueString);
        switch (fromTextMeasurement) {
            case "Kg": {
                switch (toTextMeasurement) {
                    case "Kg": {
                        return fromValue;
                    }
                    case "Pounds": {
                        return WeightConverter.kgToPounds(fromValue);
                    }
                    case "Stones": {
                        return WeightConverter.kgToStones(fromValue);
                    }
                }
                break;
            }
            case "Pounds": {
                switch (toTextMeasurement) {
                    case "Kg": {
                        return WeightConverter.poundsToKg(fromValue);
                    }
                    case "Pounds": {
                        return fromValue;
                    }
                    case "Stones": {
                        return WeightConverter.poundsToStones(fromValue);
                    }
                }
                break;
            }
            case "Stones": {
                switch (toTextMeasurement) {
                    case "Kg": {
                        return WeightConverter.stonesToKg(fromValue);
                    }
                    case "Pounds": {
                        return WeightConverter.stonesToPounds(fromValue);
                    }
                    case "Stones": {
                        return fromValue;
                    }
                }
                break;
            }
        }
        return 0.0;
    }
}
