package ro.uaic.info.megaconverter.exception;

public class MethodInvokeException extends RuntimeException {
    public MethodInvokeException(String message) {
        super(message);
    }

    public MethodInvokeException(String message, Throwable cause) {
        super(message, cause);
    }
}
