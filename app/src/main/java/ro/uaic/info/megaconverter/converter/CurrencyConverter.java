package ro.uaic.info.megaconverter.converter;

import android.app.Activity;

import ro.uaic.info.megaconverter.utils.PreferencesUtils;

public class CurrencyConverter {
    public static Double convertValue(Activity activity, String fromTextMeasurement, String toTextMeasurement, String fromValueString) {
        Double fromValue = Double.parseDouble(fromValueString);
        Double usd = PreferencesUtils.getCurrency(activity, "USD");
        Double gbp = PreferencesUtils.getCurrency(activity, "GBP");
        Double chf = PreferencesUtils.getCurrency(activity, "CHF");
        Double ron = PreferencesUtils.getCurrency(activity, "RON");
        switch (fromTextMeasurement) {
            case "EUR": {
                switch (toTextMeasurement) {
                    case "EUR": {
                        return fromValue;
                    }
                    case "USD": {
                        return fromValue * usd;
                    }
                    case "GBP": {
                        return fromValue * gbp;
                    }
                    case "CHF": {
                        return fromValue * chf;
                    }
                    case "RON": {
                        return fromValue * ron;
                    }
                }
                break;
            }
            case "USD": {
                switch (toTextMeasurement) {
                    case "EUR": {
                        return fromValue / usd;
                    }
                    case "USD": {
                        return fromValue;
                    }
                    case "GBP": {
                        return (fromValue / usd) * gbp;
                    }
                    case "CHF": {
                        return (fromValue / usd) * chf;
                    }
                    case "RON": {
                        return (fromValue / usd) * ron;
                    }
                }
                break;
            }
            case "GBP": {
                switch (toTextMeasurement) {
                    case "EUR": {
                        return fromValue / gbp;
                    }
                    case "USD": {
                        return (fromValue / gbp) * usd;
                    }
                    case "GBP": {
                        return fromValue;
                    }
                    case "CHF": {
                        return (fromValue / gbp) * chf;
                    }
                    case "RON": {
                        return (fromValue / gbp) * ron;
                    }
                }
                break;
            }
            case "CHF": {
                switch (toTextMeasurement) {
                    case "EUR": {
                        return fromValue / chf;
                    }
                    case "USD": {
                        return (fromValue / chf) * usd;
                    }
                    case "GBP": {
                        return (fromValue / chf) * gbp;
                    }
                    case "CHF": {
                        return fromValue;
                    }
                    case "RON": {
                        return (fromValue / chf) * ron;
                    }
                }
                break;
            }
            case "RON": {
                switch (toTextMeasurement) {
                    case "EUR": {
                        return fromValue / ron;
                    }
                    case "USD": {
                        return (fromValue / ron) * usd;
                    }
                    case "GBP": {
                        return (fromValue / ron) * gbp;
                    }
                    case "CHF": {
                        return (fromValue / ron) * chf;
                    }
                    case "RON": {
                        return fromValue;
                    }
                }
                break;
            }
        }
        return 0.0;
    }
}
