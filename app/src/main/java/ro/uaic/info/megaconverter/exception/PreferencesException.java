package ro.uaic.info.megaconverter.exception;

public class PreferencesException extends RuntimeException {
    public PreferencesException(String message) {
        super(message);
    }

    public PreferencesException(String message, Throwable cause) {
        super(message, cause);
    }
}
