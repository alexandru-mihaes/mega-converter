package ro.uaic.info.megaconverter.utils;

import java.util.Arrays;
import java.util.List;

public class MeasurementsStrings {
    public static List<String> modes = Arrays.asList("Length", "Temperature", "Pressure", "Weight", "Currency");

    public static List<String> lengthMeasurements = Arrays.asList("Inch", "Feet", "Meter", "KM", "Miles", "Yards");
    public static List<String> temperatureMeasurements = Arrays.asList("Celsius", "Fahrenheit", "Rankine", "Kelvin");
    public static List<String> pressureMeasurements = Arrays.asList("Pascal", "Bar", "mmHg");
    public static List<String> weightMeasurements = Arrays.asList("Kg", "Pounds", "Stones");
    public static List<String> currencyMeasurements = Arrays.asList("EUR", "USD", "GBP", "CHF", "RON");

    public static List<String> getMeasurementsByMode(String mode) {
        switch (mode) {
            case "Length": {
                return lengthMeasurements;
            }
            case "Temperature": {
                return temperatureMeasurements;
            }
            case "Pressure": {
                return pressureMeasurements;
            }
            case "Weight": {
                return weightMeasurements;
            }
            case "Currency": {
                return currencyMeasurements;
            }
        }
        return lengthMeasurements;
    }
}
