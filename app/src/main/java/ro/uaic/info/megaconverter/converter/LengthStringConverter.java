package ro.uaic.info.megaconverter.converter;

public class LengthStringConverter {
    public static Double convertValueFromTo(String fromTextMeasurement, String toTextMeasurement, String fromValueString) {
        Double fromValue = Double.parseDouble(fromValueString);
        switch (fromTextMeasurement) {
            case "Inch": {
                switch (toTextMeasurement) {
                    case "Inch": {
                        return fromValue;
                    }
                    case "Feet": {
                        return LengthConverter.inchToFeet(fromValue);
                    }
                    case "Meter": {
                        return LengthConverter.inchToMeter(fromValue);
                    }
                    case "KM": {
                        return LengthConverter.inchToKm(fromValue);
                    }
                    case "Miles": {
                        return LengthConverter.inchToMile(fromValue);
                    }
                    case "Yards": {
                        return LengthConverter.inchToYard(fromValue);
                    }
                }
                break;
            }
            case "Feet": {
                switch (toTextMeasurement) {
                    case "Inch": {
                        return LengthConverter.feetToInch(fromValue);
                    }
                    case "Feet": {
                        return fromValue;
                    }
                    case "Meter": {
                        return LengthConverter.feetToMeter(fromValue);
                    }
                    case "KM": {
                        return LengthConverter.feetToKm(fromValue);
                    }
                    case "Miles": {
                        return LengthConverter.feetToMile(fromValue);
                    }
                    case "Yards": {
                        return LengthConverter.feetToYard(fromValue);
                    }
                }
                break;
            }
            case "Meter": {
                switch (toTextMeasurement) {
                    case "Inch": {
                        return LengthConverter.meterToInch(fromValue);
                    }
                    case "Feet": {
                        return LengthConverter.meterToFeet(fromValue);
                    }
                    case "Meter": {
                        return fromValue;
                    }
                    case "KM": {
                        return LengthConverter.meterToKm(fromValue);
                    }
                    case "Miles": {
                        return LengthConverter.meterToMile(fromValue);
                    }
                    case "Yards": {
                        return LengthConverter.meterToYard(fromValue);
                    }
                }
                break;
            }
            case "KM": {
                switch (toTextMeasurement) {
                    case "Inch": {
                        return LengthConverter.kmToInch(fromValue);
                    }
                    case "Feet": {
                        return LengthConverter.kmToFeet(fromValue);
                    }
                    case "Meter": {
                        return LengthConverter.kmToMeter(fromValue);
                    }
                    case "KM": {
                        return fromValue;
                    }
                    case "Miles": {
                        return LengthConverter.kmToMile(fromValue);
                    }
                    case "Yards": {
                        return LengthConverter.kmToYard(fromValue);
                    }
                }
                break;
            }
            case "Miles": {
                switch (toTextMeasurement) {
                    case "Inch": {
                        return LengthConverter.mileToInch(fromValue);
                    }
                    case "Feet": {
                        return LengthConverter.mileToFeet(fromValue);
                    }
                    case "Meter": {
                        return LengthConverter.mileToMeter(fromValue);
                    }
                    case "KM": {
                        return LengthConverter.mileToKm(fromValue);
                    }
                    case "Miles": {
                        return fromValue;
                    }
                    case "Yards": {
                        return LengthConverter.mileToYard(fromValue);
                    }
                }
                break;
            }
            case "Yards": {
                switch (toTextMeasurement) {
                    case "Inch": {
                        return LengthConverter.yardToInch(fromValue);
                    }
                    case "Feet": {
                        return LengthConverter.yardToFeet(fromValue);
                    }
                    case "Meter": {
                        return LengthConverter.yardToMeter(fromValue);
                    }
                    case "KM": {
                        return LengthConverter.yardToKm(fromValue);
                    }
                    case "Miles": {
                        return LengthConverter.yardToMile(fromValue);
                    }
                    case "Yards": {
                        return fromValue;
                    }
                }
                break;
            }
        }
        return 0.0;
    }
}
