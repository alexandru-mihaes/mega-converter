package ro.uaic.info.megaconverter.exception;

public class JsonKeyException extends RuntimeException {
    public JsonKeyException(String message) {
        super(message);
    }

    public JsonKeyException(String message, Throwable cause) {
        super(message, cause);
    }
}
