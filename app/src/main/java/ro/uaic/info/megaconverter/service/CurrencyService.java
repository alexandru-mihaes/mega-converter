package ro.uaic.info.megaconverter.service;

import android.app.Activity;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import ro.uaic.info.megaconverter.network.ServerUtils;
import ro.uaic.info.megaconverter.utils.PreferencesUtils;

public class CurrencyService extends BaseService {
    private static final String TAG_LOGGING = CurrencyService.class.getName();

    public CurrencyService(Activity activity) {
        super(activity, ServerUtils.getLatestForeignExchangeRates());
    }

    public void makeRequest() {
        super.makeRequest(
                Request.Method.GET,
                this.getClass()
        );
    }

    @Override
    protected void handleSuccess(JSONObject response) {
        Log.i(TAG_LOGGING, "Currency request successfully made");
        PreferencesUtils.saveCurrencies(activity, response);
    }

    @Override
    protected void handleError(VolleyError error) {
        super.handleError(error);
    }
}