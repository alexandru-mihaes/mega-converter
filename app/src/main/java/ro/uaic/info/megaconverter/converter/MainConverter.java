package ro.uaic.info.megaconverter.converter;

public class MainConverter {
    public static Double convertValueFromToByMode(String fromTextMeasurement, String toTextMeasurement, String fromValueString, String mode) {
        if(mode.equals("Length")) {
            return LengthStringConverter.convertValueFromTo(fromTextMeasurement, toTextMeasurement, fromValueString);
        } else if (mode.equals("Temperature")) {
            return TemperatureStringConverter.convertValueFromTo(fromTextMeasurement, toTextMeasurement, fromValueString);
        } else if (mode.equals("Pressure")) {
            return PressureStringConverter.convertValueFromTo(fromTextMeasurement, toTextMeasurement, fromValueString);
        } else if (mode.equals("Weight")) {
            return WeightStringConverter.convertValueFromTo(fromTextMeasurement, toTextMeasurement, fromValueString);
        }
        return 0.0;
    }
}
