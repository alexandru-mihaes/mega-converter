package ro.uaic.info.megaconverter.converter;

public class LengthConverter {
    /* Inch */
    public static double inchToFeet(double inch) {
        return inch * 0.0833333333;
    }

    public static double feetToInch(double feet) {
        return feet / 0.0833333333;
    }

    public static double inchToMeter(double inch) {
        return inch * 0.0254;
    }

    public static double meterToInch(double meter) {
        return meter / 0.0254;
    }

    public static double inchToKm(double inch) {
        return meterToKm(inchToMeter(inch));
    }

    public static double kmToInch(double km) {
        return meterToInch(kmToMeter(km));
    }

    public static double inchToMile(double inch) {
        return inch / 63360;
    }

    public static double mileToInch(double mile) {
        return mile * 63360;
    }

    public static double inchToYard(double inch) {
        return meterToYard(inchToMeter(inch));
    }

    public static double yardToInch(double yard) {
        return meterToInch(yardToMeter(yard));
    }

    /* Feet */
    public static double feetToMeter(double feet) {
        return feet * 0.3048;
    }

    public static double meterToFeet(double meter) {
        return meter / 0.3048;
    }

    public static double feetToKm(double feet) {
        return meterToKm(feetToMeter(feet));
    }

    public static double kmToFeet(double km) {
        return meterToFeet(kmToMeter(km));
    }

    public static double feetToMile(double feet) {
        return feet / 5280;
    }

    public static double mileToFeet(double mile) {
        return mile * 5280;
    }

    public static double feetToYard(double feet) {
        return meterToYard(feetToMeter(feet));
    }

    public static double yardToFeet(double yard) {
        return meterToFeet(yardToMeter(yard));
    }

    /* Meter */
    public static double meterToKm(double meter) {
        return meter /1000;
    }

    public static double kmToMeter(double km) {
        return km * 1000;
    }


    public static double meterToMile(double meter) {
        return meter * 1609.344;
    }

    public static double mileToMeter(double mile) {
        return mile / 1609.344;
    }

    public static double meterToYard(double meter) {
        return meter * 1.0936;
    }

    public static double yardToMeter(double yard) {
        return yard / 1.0936;
    }

    /* KM */
    public static double kmToMile(double km) {
        return meterToMile(kmToMeter(km));
    }

    public static double mileToKm(double mile) {
        return meterToKm(mileToMeter(mile));
    }

    public static double kmToYard(double km) {
        return meterToYard(kmToMeter(km));
    }

    public static double yardToKm(double yard) {
        return meterToKm(yardToMeter(yard));
    }

    /* Mile */
    public static double mileToYard(double mile) {
        return meterToYard(mileToMeter(mile));
    }

    public static double yardToMile(double yard) {
        return meterToMile(yardToMeter(yard));
    }
}
