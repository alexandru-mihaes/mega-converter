package ro.uaic.info.megaconverter.converter;

class PressureConverter {
    /* Pascal */
    public static Double pascalToBar(Double pascal) {
        return pascal / 100000;
    }

    public static Double barToPascal(Double bar) {
        return bar * 100000;
    }

    public static Double pascalToMmhg(Double pascal) {
        return pascal * 0.00750061683;
    }

    public static Double mmhgToPascal(Double mmhg) {
        return mmhg / 0.00750061683;
    }

    /* Bar */
    public static Double barToMmhg(Double bar) {
        return bar * 750.061683;
    }

    public static Double mmhgToBar(Double mmhg) {
        return mmhg / 750.061683;
    }
}
