package ro.uaic.info.megaconverter.service;

import android.app.Activity;
import android.util.Log;
import android.util.Pair;
import android.view.View;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;

import ro.uaic.info.megaconverter.exception.JsonKeyException;
import ro.uaic.info.megaconverter.exception.MethodInvokeException;
import ro.uaic.info.megaconverter.network.VolleyRequest;

public abstract class BaseService {
    private static final String TAG_LOGGING = BaseService.class.getName();

    protected final Activity activity;
    protected final String URL_STRING;

    public BaseService(Activity activity, String URL_STRING) {
        this.activity = activity;
        this.URL_STRING = URL_STRING;
    }

    public void makeRequest(int requestMethod, Class classHandler, Pair<String, String>... jsonPairs) {
        try {
            JSONObject jsonObject = new JSONObject();
            for (Pair<String, String> pair : jsonPairs) {
                jsonObject.put(pair.first, pair.second);
            }

            VolleyRequest volleyRequest = new VolleyRequest(
                    activity,
                    URL_STRING,
                    requestMethod,
                    jsonObject
            );

            // setting the success & error methods
            volleyRequest.setObjectToInvokeOn(this);
            Class[] successParameters = new Class[1];
            successParameters[0] = JSONObject.class;
            Method successLoginMethod = classHandler.getMethod("handleSuccess", successParameters);
            Class[] errorParameters = new Class[1];
            errorParameters[0] = VolleyError.class;
            Method errorLoginMethod = classHandler.getMethod("handleError", errorParameters);
            volleyRequest.setOnSuccessMethod(successLoginMethod);
            volleyRequest.setOnErrorMethod(errorLoginMethod);

            volleyRequest.invokeRequest();
        } catch (NoSuchMethodException e) {
            throw new MethodInvokeException(e.getMessage());
        } catch (JSONException e) {
            throw new JsonKeyException(e.getMessage());
        }
    }

    protected void handleSuccess(JSONObject response) {
        Log.i(TAG_LOGGING, "Default success handling");
    }

    protected void handleError(VolleyError error) {
        Log.e(TAG_LOGGING, "Default error handling");
    }
}
