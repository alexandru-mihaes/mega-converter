package ro.uaic.info.megaconverter.converter;

class WeightConverter {
    /* Kg */
    public static double kgToPounds(double kg) {
        return kg / 0.45359237;
    }

    public static double poundsToKg(double pounds) {
        return pounds * 0.45359237;
    }

    public static double kgToStones(double kg) {
        return poundsToStones(kgToPounds(kg));
    }

    public static double stonesToKg(double stones) {
        return poundsToKg(stonesToPounds(stones));
    }

    /* Pounds */
    public static double poundsToStones(double pounds) {
        return pounds / 14;
    }

    public static double stonesToPounds(double stones) {
        return stones * 14;
    }
}
