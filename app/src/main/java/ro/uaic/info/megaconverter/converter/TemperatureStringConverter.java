package ro.uaic.info.megaconverter.converter;

class TemperatureStringConverter {
    public static Double convertValueFromTo(String fromTextMeasurement, String toTextMeasurement, String fromValueString) {
        Double fromValue = Double.parseDouble(fromValueString);
        switch (fromTextMeasurement) {
            case "Celsius": {
                switch (toTextMeasurement) {
                    case "Celsius": {
                        return fromValue;
                    }
                    case "Fahrenheit": {
                        return TemperatureConverter.celsiusToFahrenheit(fromValue);
                    }
                    case "Rankine": {
                        return TemperatureConverter.celsiusToRankine(fromValue);
                    }
                    case "Kelvin": {
                        return TemperatureConverter.celsiusToKelvin(fromValue);
                    }
                }
                break;
            }
            case "Fahrenheit": {
                switch (toTextMeasurement) {
                    case "Celsius": {
                        return TemperatureConverter.fahrenheitToCelsius(fromValue);
                    }
                    case "Fahrenheit": {
                        return fromValue;
                    }
                    case "Rankine": {
                        return TemperatureConverter.fahrenheitToRankine(fromValue);
                    }
                    case "Kelvin": {
                        return TemperatureConverter.fahrenheitToKelvin(fromValue);
                    }
                }
                break;
            }
            case "Rankine": {
                switch (toTextMeasurement) {
                    case "Celsius": {
                        return TemperatureConverter.rankineToCelsius(fromValue);
                    }
                    case "Fahrenheit": {
                        return TemperatureConverter.rankineToFahrenheit(fromValue);
                    }
                    case "Rankine": {
                        return fromValue;
                    }
                    case "Kelvin": {
                        return TemperatureConverter.rankineToKelvin(fromValue);
                    }
                }
                break;
            }
            case "Kelvin": {
                switch (toTextMeasurement) {
                    case "Celsius": {
                        return TemperatureConverter.kelvinToCelsius(fromValue);
                    }
                    case "Fahrenheit": {
                        return TemperatureConverter.kelvinToFahrenheit(fromValue);
                    }
                    case "Rankine": {
                        return TemperatureConverter.kelvinToRankine(fromValue);
                    }
                    case "Kelvin": {
                        return fromValue;
                    }
                }
                break;
            }
        }
        return 0.0;
    }
}
