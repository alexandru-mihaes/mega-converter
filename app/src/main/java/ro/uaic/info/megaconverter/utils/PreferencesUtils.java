package ro.uaic.info.megaconverter.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import ro.uaic.info.megaconverter.exception.PreferencesException;

public class PreferencesUtils {
    private static final String TAG_LOGGING = PreferencesUtils.class.getName();

    private static final String preferencesFileKey = "ro.uaic.info.megaconverter";

    public static void saveCurrencies(Activity activity, JSONObject response) {
        try {
            JSONObject rates = response.getJSONObject("rates");
            String usd = rates.getString("USD");
            String gbp = rates.getString("GBP");
            String chf = rates.getString("CHF");
            String ron = rates.getString("RON");
            SharedPreferences preferences = activity.getSharedPreferences(preferencesFileKey, Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = preferences.edit();
            edit.putString("USD", usd);
            edit.putString("GBP", gbp);
            edit.putString("CHF", chf);
            edit.putString("RON", ron);
            Log.i(TAG_LOGGING, "Saving currencies to shared preferences");
            edit.apply();
        } catch (JSONException e) {
            Log.e(TAG_LOGGING, "Cannot save currencies to shared preferences");
        }
    }

    public static Double getCurrency(Activity activity, String currency) throws PreferencesException {
        SharedPreferences prefs = activity.getSharedPreferences(preferencesFileKey, Context.MODE_PRIVATE);
        String usd = prefs.getString("USD", "");
        String gbp = prefs.getString("GBP", "");
        String chf = prefs.getString("CHF", "");
        String ron = prefs.getString("RON", "");
        if (usd == null || gbp == null || chf == null || ron == null
                || usd.isEmpty() || gbp.isEmpty() || chf.isEmpty() || ron.isEmpty()) {
            Log.e(TAG_LOGGING, "Currencies not found in shared preferences");
            throw new PreferencesException("Empty currencies data");
        }
        Log.i(TAG_LOGGING, "Currencies found in shared preferences");
        switch (currency) {
            case "USD": {
                return Double.parseDouble(usd);
            }
            case "GBP": {
                return Double.parseDouble(gbp);
            }
            case "CHF": {
                return Double.parseDouble(chf);
            }
            case "RON": {
                return Double.parseDouble(ron);
            }
        }
        Log.e(TAG_LOGGING, "Currency not present");
        return 0.0;
    }
}
