package ro.uaic.info.megaconverter.converter;

public class TemperatureConverter {
    /* Celsius */
    public static double celsiusToFahrenheit(double celsius) {
        return celsius * (9 / 5.0) + 32;
    }

    public static double fahrenheitToCelsius(double fahrenheit) {
        return (fahrenheit - 32) * (5 / 9.0);
    }

    public static double celsiusToRankine(double celsius) {
        return (celsius + 273.15) * (9 / 5.0);
    }

    public static double rankineToCelsius(double rankine) {
        return (rankine - 491.67) * (5 / 9.0);
    }

    public static double celsiusToKelvin(double celsius) {
        return celsius + 273.15;
    }

    public static double kelvinToCelsius(double kelvin) {
        return kelvin - 273.15;
    }

    /* Fahrenheit */
    public static double fahrenheitToRankine(double fahrenheit) {
        return fahrenheit + 459.67;
    }

    public static double rankineToFahrenheit(double rankine) {
        return rankine - 459.67;
    }

    public static double fahrenheitToKelvin(double fahrenheit) {
        return (fahrenheit + 459.67) * (5 / 9.0);
    }

    public static double kelvinToFahrenheit(double kelvin) {
        return kelvin * (9/5.0) - 459.67;
    }

    /* Rankine */
    public static double rankineToKelvin(double kelvin) {
        return kelvin * (5/9.0);
    }

    public static double kelvinToRankine(double kelvin) {
        return kelvin * (9/5.0);
    }
}
