package ro.uaic.info.megaconverter.network;

import android.app.Activity;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import ro.uaic.info.megaconverter.exception.VolleyRequestException;

public class VolleyRequest {
    private final Activity activity;
    private final String URL_STRING;
    private final int REQUEST_METHOD_CODE;
    private JSONObject jsonBodyObj;
    private JsonObjectRequest jsonObjectRequest;
    private Object objectToInvokeOn;
    private Method onSuccessMethod;
    private Method onErrorMethod;

    public VolleyRequest(final Activity activity,
                         final String URL_STRING,
                         final int REQUEST_METHOD_CODE,
                         JSONObject jsonBodyObj) {
        this.activity = activity;
        this.URL_STRING = URL_STRING;
        this.REQUEST_METHOD_CODE = REQUEST_METHOD_CODE;
        this.jsonBodyObj = jsonBodyObj;
    }

    public void setObjectToInvokeOn(Object objectToInvokeOn) {
        this.objectToInvokeOn = objectToInvokeOn;
    }

    public void setOnSuccessMethod(Method method) {
        onSuccessMethod = method;
    }

    public void setOnErrorMethod(Method method) {
        onErrorMethod = method;
    }

    public void invokeRequest() {
        createRequest();
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(jsonObjectRequest);
    }

    private void createRequest() {
        final String requestBody = jsonBodyObj.toString();
        jsonObjectRequest = new JsonObjectRequest(
                REQUEST_METHOD_CODE,
                URL_STRING,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            onSuccessMethod.invoke(objectToInvokeOn, response);
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            throw new VolleyRequestException(e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            onErrorMethod.invoke(objectToInvokeOn, error);
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            throw new VolleyRequestException(e.getMessage());
                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

            @Override
            public byte[] getBody() {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                            requestBody, "utf-8");
                    return null;
                }
            }
        };
    }
}
