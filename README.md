![Preview](assets/images/preview.png)

A simple converter app for Android

## Features:
- support for physic measures (length, temperature, pressure, weight) and several currencies
- the exchange rates are automatically feeded
- the last rates are stored persistent after the first request
- the exchange rates used are pulled from [Exchange rates API](https://exchangeratesapi.io/) published by the [European Central Bank](https://www.ecb.europa.eu/stats/policy_and_exchange_rates/euro_reference_exchange_rates/html/index.en.html)